pipeline {
	
  agent any
  		
  environment {
    Settings = "--settings ${env.PROJECT_FILE_SETTINGS}"
    SonarHost = "${env.QUALITY_REPO_PROTOCOL}${env.QUALITY_REPO_HOST}:${env.QUALITY_REPO_PORT}"
    RepoHost = "${env.PROJECT_REPO_HOST}/${env.PROJECT_REPO_USER}/${env.PROJECT_REPO_NAME}.git"
	NexusServer = "${env.ARTIFACT_REPO_PROTOCOL}${env.ARTIFACT_REPO_HOST}:${env.ARTIFACT_REPO_PORT}"
	EmailRecipients = "${env.MAIL_RECIPIENTS}"         
  }

  options {
    buildDiscarder(logRotator(numToKeepStr: '10')) 
    disableConcurrentBuilds() 
    disableResume()
    timeout(time: 1, unit: 'DAYS')
	durabilityHint('PERFORMANCE_OPTIMIZED')
       
  }

  stages {

    stage("Commit") {
       steps {
	    
          runStepsCommitStage()

       }                   
    }
            
    stage("Aceitação") {
       steps {		   			   

 	 runStepsAceitacaoStage()

       }
    }

    stage("Homolocação") {
       steps {

 	  runStepsHomolocacaoStage()

       }
    }
	 
    stage("Produção") {
      steps {

         runStepsProducaoStage()

      }
    }

  } 
  
  post {
	  always {
		deleteDir()
		enviarNotificacao()
	  }
		
	  success {
		echo "Pipeline concluído com sucesso"
	  }
		
	  unstable {
		 echo "Pipeline apresenta instabilidade"
	  }
		
	  failure {
		echo "Erro na execução do pipeline"
	  }
	  changed {
		 echo "O pipeline teve a execução atual diferente da última"
	  }
	}

} //end pipeline

void runStepsCommitStage() {
	
  withCredentials([usernamePassword(credentialsId: 'repository', passwordVariable: 'PROJECT_REPO_PASSWORD', usernameVariable: 'PROJECT_REPO_USER')]) {
    sh "git config --global credential.username ${PROJECT_REPO_USER}"			 
    sh "git config --global credential.helper '!echo password=${PROJECT_REPO_PASSWORD}; echo'"										
  }
			

  script {
    if (env.MULTI_BRANCH_PIPELINE == "True") {
      selecionarBranch()
    }
    else 
    {env.BRANCH_SELECIONADA = 'master'}
   }
     					 
  def tagName  				
  script {
    if (env.BRANCH_SELECIONADA == 'master') {
	  sh "git checkout -b $PROJECT_NAME-R-$VERSION_NUMBER-$BUILD_NUMBER"
	  tagName = "$VERSION_NUMBER-$BUILD_NUMBER"
	}
	else {
	  sh 'git checkout "$BRANCH_SELECIONADA"'
	  Branch = "${env.BRANCH_SELECIONADA}"
	  Projeto = "${env.PROJECT_NAME}"
	  NumeroBuild = "${env.BUILD_NUMBER}"
	  tagName = Branch.substring(Projeto.length()+2,Branch.length())+"-fix-"+NumeroBuild
	}
  }
					 
  sh "mvn versions:set -DnewVersion=$VERSION_NUMBER-$BUILD_NUMBER ${Settings}"  
  sh "mvn clean install ${Settings}"				  
  
  script {
	  if (env.QUALITY_REPO_ENABLED == 'True')
		{ sh "mvn sonar:sonar -Dsonar.host.url=${SonarHost} ${Settings}"}
	}  
  
  sh "git commit -a -m 'Nova release candidata'"
  sh "git tag -a ${tagName} --force -m 'Nova versão'"
  sh "git push https://${PROJECT_REPO_USER}@${RepoHost} --follow-tags"

  sh "cp target/${PROJECT_NAME}.war ."
  sh "cp ambiente/wildfly/Dockerfile ."
    
  def imagemComNomeCompleto = "${env.IMAGE_REPO_USER}/${env.PROJECT_NAME}:${tagName}"
   
  def dockerCommand = "docker build -t "+imagemComNomeCompleto+" -f Dockerfile ."
  sh dockerCommand
    
  imageRepository = "${env.IMAGE_REPO_PROTOCOL}${env.IMAGE_REPO_HOST}"

  withCredentials([usernamePassword(credentialsId: 'image-repository', passwordVariable: 'ImageRepoPassword', usernameVariable: 'ImageRepoUser')]) {
	
    docker.withRegistry(imageRepository, 'image-repository') {  
	  dockerCommand = "docker push "+imagemComNomeCompleto
	  sh "docker login -u ${ImageRepoUser} -p ${ImageRepoPassword}"
      sh dockerCommand
    }
  }
  
  script {
	if (env.APP_SERVER_DEV_ENABLED == 'True') {
	  realizarDeploySSH("ambiente/.env-desenvolvimento","ssh-desenvolvimento", imagemComNomeCompleto)
	}
  }
  	
}

void runStepsAceitacaoStage() {
	
  script {
	if (env.APP_SERVER_TEST_ENABLED == 'True') {
	  realizarDeploySSH("ambiente/.env-teste","ssh-teste", imagemComNomeCompleto)
      sh "mvn failsafe:integration-test ${Settings}"
	}
  }
	  
  input ( message: 'Autoriza promoção da build para homologação?', ok: 'Autorizado', submitter: 'admin' )
	
}

void runStepsHomolocacaoStage() {
	
  script {
	if (env.APP_SERVER_HOM_ENABLED == 'True') {
	  realizarDeploySSH("ambiente/.env-homologacao","ssh-homologacao", imagemComNomeCompleto)
	} 
  }
	 
  input ( message: 'Autoriza promoção da build para produção?', ok: 'Autorizado', submitter: 'admin' )
	
}

void runStepsProducaoStage() {
	
  script {
	if (env.APP_SERVER_PROD_ENABLED == 'True') {
	  realizarDeploySSH("ambiente/.env-producao","ssh-producao", imagemComNomeCompleto)
	}
  }

}

String carregarVariavelAmbiente(String parametro, String arquivo) {
	
	valorParametro = sh (
		script: "cat ${arquivo} | grep ${parametro} | cut -d'=' -f2-",
		returnStdout: true
	)
	
	return valorParametro
			 
}

void enviarNotificacao() {
	
	def jobName = currentBuild.fullDisplayName
	emailext body: '''${SCRIPT, template="email.template"}''',
    subject: "[Jenkins] ${jobName}",
    to: "${EmailRecipients}",
    replyTo: "${EmailRecipients}",
    recipientProviders: [[$class: 'CulpritsRecipientProvider']]
}

void selecionarBranch() {
	
	sh 'git branch -r | awk \'{print $1}\' ORS=\'\\n\' >branches.txt'	
	sh '''cut -d '/' -f 2 branches.txt > branch.txt'''	 
	branches = readFile "branch.txt"	

	def didTimeout = false
	try {
	  timeout(time: 60, unit: 'SECONDS') {
		env.BRANCH_SELECIONADA = input (
		   id: 'SelecaoBranchParaPipeline', message: 'Escolha a branch para execução do pipeline', 
		   parameters: [choice(name: 'BRANCH_NAME', choices: "${branches}", description: 'Branches disponíveis')]
		   )
	  }
	} catch(err) {
		def user = err.getCauses()[0].getUser()
		if('SYSTEM' == user.toString()) { // SYSTEM means timeout
			env.BRANCH_SELECIONADA = "master"
		} else {
			userInput = false
			echo "Aborted by: [${user}]"
		}
	}
	
}

void realizarDeploySSHPluginSteps(String arquivoConfiguracao, String credencial, String imagem) {
  def remote = [:]
  remote.allowAnyHosts = true
  remote.name = carregarVariavelAmbiente("APP_SERVER_USER", arquivoConfiguracao)
  remote.host = carregarVariavelAmbiente("APP_SERVER_HOST", arquivoConfiguracao)
  remote.user = carregarVariavelAmbiente("APP_SERVER_USER", arquivoConfiguracao)
  port = carregarVariavelAmbiente("APP_SERVER_PORT", arquivoConfiguracao)
  runContainerCommand = "docker run -d --env-file=$arquivoConfiguracao --expose=$port $imagem"
  withCredentials([sshUserPrivateKey(credentialsId: credencial, keyFileVariable: 'keyfile')]) {	
	sshPut remote: remote, from: arquivoConfiguracao, into: '.'
	sshCommand remote: remote, command: runContainerCommand
  }
}
  
void realizarDeploySSHPluginPublishOverSSH(String arquivoConfiguracao, String credencial, String imagem) {
  host = carregarVariavelAmbiente("APP_SERVER_HOST", arquivoConfiguracao)
  user = carregarVariavelAmbiente("APP_SERVER_USER", arquivoConfiguracao)
  	
  //sh "scp -v ${arquivoConfiguracao} ${user}@${host}:/"
  sh "scp -v $arquivoConfiguracao ip172-18-0-15-bm1nd5rn838g00a7bu2g@direct.labs.play-with-docker.com:/root"
  
  sshPublisher(
	publishers: [
	  sshPublisherDesc(
		configName: $host, 
		transfers: [
		  sshTransfer(
			sourceFiles: arquivoConfiguracao,
			execCommand: "ls -la"			
		  )],
	    verbose: true)])
}

void realizarDeploySSHPluginSSH(String arquivoConfiguracao, String credencial, String imagem) {
	host = carregarVariavelAmbiente("APP_SERVER_HOST", arquivoConfiguracao)
	user = carregarVariavelAmbiente("APP_SERVER_USER", arquivoConfiguracao)
		
	//sh "scp -v ${arquivoConfiguracao} ${user}@${host}:/"
	sh "scp -v $arquivoConfiguracao ip172-18-0-15-bm1nd5rn838g00a7bu2g@direct.labs.play-with-docker.com:/root"
	
	/*
	valorParametro = sh (
		script: "cat ${arquivo} | grep ${parametro} | cut -d'=' -f2-",
		returnStdout: true
	)
	*/
	
  }
